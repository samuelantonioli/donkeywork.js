
/*
  requires Zepto/jQuery
  function overlay(m,b,t,c) {
    check buttons
    create overlay or use existing
    create popup in overlay (set zzindex)
    append t in popup
    append m in popup
    for i in b
        b[i].click = b[i]?b[i]:
        function(){
            if overlay.last_popup?
                remove this overlay
            else remove this popup
        }
    }
    overlay('MSG',{Ok:null},'TIT');
    overlay('MSG2',{Ok:null},'TIT2');
    overlay('MSG3',{Ok:null},'TIT3');
    __CSS__
    .overlay {
        z-index:10;
        position: fixed;
        left: 0px;
        top: 0px;
        width:100%;
        height:100%;
        background:rgba(92,92,92,0.5);
    }
    .popup {
        border-radius:5px;
        padding:5px;
        background:#fff;
        width:820px;
        margin:100px auto;
    }
    .popupinner {
        max-height:250px;
        overflow:auto;
    }
*/
function overlay(m,b,t,c){
    if (!(function(b,k){
        for(k in b)
            return true;
        return false;
    })(b)) return;
    var k,e,i,o='#overlay',s='<hr>',
        p=$('<div class="popup">');
    if(!$(o).length){
        $(c||document.body)
            .append('<div id="overlay" class="overlay">');
    }o=$(o);
    //p.css('z-index',$('.popup').length+1);
    if(t)p.append($('<h1>').wrapInner(t));
    if(t&&s)p.append(s);
    if(m)p.append($('<div class="popupinner">').wrapInner(m));
    if(m&&s)p.append(s);
    for(k in b){
        i=$('<input type="button">')
        i.attr('value',k);
        i.on('click', function(f,o){return function(){
            if (!f||f.call($(this).parent())===false) {
                $(this).parent().remove();
                if (!$('.popup').length) {
                    o.empty().hide();
                }else{
                    $('.popup').first().show();
                }
            }
        }}(b[k],o));
        p.append(i);
    }
    o.show();
    $('.popup').hide();
    o.prepend(p);
    p.show();
}
