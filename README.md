# DonkeyWork.JS

Some Javascript Snippets (tested on FF)

# **element.js**

`element(html_as_json) => [DOM Element]`

## Tag Structure

         * [
         *  'tag' || element,
         *  {attr:'val'} || none,
         *  obj.toString() || none,
         *  [
         *   ['tag',...],
         *   ...
         *  ] || none
         * ]

## Example
input

        ['div', {id:'somediv'}, null, [
            ['p', null, 'test'],
            ['div', {id:'otherdiv'}, null, [
                ['p', null, 'othertest']
            ]],
            ['p', null, 'third']
        ]]

output

        [Object HTMLDivElement]
        <div id="somediv">
            <p>test</p>
            <div id="otherdiv">
                <p>othertest</p>
            </div>
            <p>third</p>
        </div>

call examples

        element([
            'ul', {id:'id'}, 'optional text',
            [
                ['li', null, 'Hello World!']
            ]
        ])

        $(document.body).append(element([$('#obj')[0], {id:'editor'}, null, [
            ['input', {id:'id_', type:'text', placeholder:'test'}]
        ]]))

# **overlay.js**

Supports multiple Overlay - Div - Elements. Requires `Zepto` or `jQuery`.  
Style `#overlay`, `.popup` and `.popupinner` to use this Javascript.  
You can modify the class/id names in the function.

# `overlay(msg, buttons, element=body)`

Set html with `msg` in popup and append buttons with actions.  
If a button returns `false`, the popup closes.

# Example

    overlay('Are you sure?', {
        'Yes': function() {
            overlay('Delete success',{'Ok':null});
            return false;
        },
        'No': function() {
            overlay('Deletion cancelled',{'Ok':null},page);
            return false;
        },
        'Maybe': function() {
            overlay('<strong>You don\'t know?</strong>',{'Yes':null});
            return false;
        }
    });

# Example Markup

    #overlay: {
        position: fixed;
        left: 0px;
        top: 0px;
        width:100%;
        height:100%;
        background:rgba(92,92,92,0.1);
        zoom: 1;
	    filter: alpha(opacity=100);
	    opacity: 1.0;
    }
    .popup {
        width:80%;
        max-height:70%;
        margin: 100px auto;
        overflow:auto;
    }
    .popupinner {
        /* ... */
    }

# **majax.js**

Simple Ajax Request

# `majax(url, callback_success, callback_error, async=true, param)`

If `param` set, then HTTP `POST` will be used, otherwise `GET`.  
`callback_success` gets the `req.responseText` as argument.
